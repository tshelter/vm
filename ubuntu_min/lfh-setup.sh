#!/bin/bash

mkdir -p /var/run/sshd

if [[ -f /root/.ssh/authorized_keys && -f /etc/ssh/sshd_config.d/00-lfh-setup.conf ]]; then
  echo "LFH setup is already done."
  exit 0
fi

get_env_value() {
  local var_name="$1"
  local pid=1

  local env_file="/proc/$pid/environ"
  local var_value # SC2155
  var_value=$(tr '\0' '\n' < "$env_file" | grep "^$var_name=" | cut -d'=' -f2-)

  # Return the value if found
  echo "$var_value"
}

SSH_PUBLIC_KEY=$(get_env_value "SSH_PUBLIC_KEY")
SSH_PORT=$(get_env_value "SSH_PORT")

mkdir -p /root/.ssh
echo "$SSH_PUBLIC_KEY" > /root/.ssh/authorized_keys
echo "Port $SSH_PORT" > /etc/ssh/sshd_config.d/00-lfh-setup.conf
