#!/usr/bin/env bash
FULL_PATH_TO_SCRIPT="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$FULL_PATH_TO_SCRIPT")"
cd "$SCRIPT_DIRECTORY" || (echo "Failed to cd to $SCRIPT_DIRECTORY" && exit 1)

docker-compose up -d --remove-orphans
