#!/usr/bin/env bash
FULL_PATH_TO_SCRIPT="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$FULL_PATH_TO_SCRIPT")"
cd "$SCRIPT_DIRECTORY" || (echo "Failed to cd to $SCRIPT_DIRECTORY" && exit 1)

podman-compose -f base.yaml build ubuntu_min
podman-compose -f base.yaml build ubuntu
